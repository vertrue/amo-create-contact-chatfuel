import vibe.utils.dictionarylist;
import std.stdio;
import std.uuid;
import std.string;
import std.conv;
import vibe.http.server;
import vibe.http.router;
import core.time;
import vibe.core.core;
import vibe.core.log;
import vibe.data.json;
import vibe.http.client;
import std.datetime.systime;
import std.file;
import std.net.curl;
import std.json;
import std.ascii : LetterCase;
import std.digest;
import std.digest.md;
import std.digest.sha;
import std.string : representation;
import std.digest.hmac;
import std.base64;
import vibe.data.serialization;
import std.uri;
import vibe.http.common;

void main()
{
	auto router = new URLRouter;
	router
		.post("/contact",&contact);
	auto l = listenHTTP("0.0.0.0:8233", router);
	runApplication();
	scope (exit) {
	    exitEventLoop(true);
	    sleep(1.msecs);
    }
    l.stopListening();
}

void contact(HTTPServerRequest req_end, HTTPServerResponse res_end)
{
	string session_id, user_lang, contact_id;
	requestHTTP("https://" ~ req_end.json["subdomain"].get!string ~ ".amocrm.ru/private/api/auth.php?type=json",					
			(scope req) {
				req.method = HTTPMethod.POST;
				Json post = Json.emptyObject;
				post["USER_LOGIN"] = req_end.json["USER_LOGIN"].get!string;
				post["USER_HASH"] = req_end.json["USER_HASH"].get!string;
				req.writeJsonBody(post);
			},
			(scope res){
				auto cookies = res.cookies();
				session_id = cookies["session_id"].value;
				user_lang = cookies["user_lang"].value;
			}
		);

	requestHTTP("https://" ~ req_end.json["subdomain"].get!string ~".amocrm.ru/api/v2/contacts",					
			(scope req) {
				req.method = HTTPMethod.POST;
				req.headers["Cookie"] = "session_id=" ~ session_id ~ "; " ~ "user_lang=" ~ user_lang;
				Json post = Json.emptyObject;
				post["add"] = req_end.json["add"];
				req.writeJsonBody(post);
			},
			(scope res){
				Json resp = res.readJson;
				contact_id = resp["_embedded"]["items"][0]["id"].to!string;
			}
		);

	requestHTTP("https://" ~ req_end.json["subdomain"].get!string ~".amocrm.ru/api/v2/leads",					
			(scope req) {
				req.method = HTTPMethod.POST;
				req.headers["Cookie"] = "session_id=" ~ session_id ~ "; " ~ "user_lang=" ~ user_lang;
				Json post = Json.emptyObject;
				post["add"] = Json.emptyArray;

				Json add = Json.emptyObject;
				add["name"] = req_end.json["lead_name"].to!string;
				add["contacts_id"] = Json.emptyArray;

				Json contact_ = Json.emptyArray;
				contact_ ~= contact_id;

				add["contacts_id"] ~= contact_;
				add["custom_fields"] = req_end.json["custom_fields"];

				post["add"] ~= add;

				req.writeJsonBody(post);
			},
			(scope res){
			}
		);
}